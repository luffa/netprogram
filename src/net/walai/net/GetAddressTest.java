package net.walai.net;
import java.net.*;
public class GetAddressTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			InetAddress a = InetAddress.getLocalHost();
			byte b[] = a.getAddress();
			for(int i=0;i<b.length;i++){
				int x=b[i]<0 ? b[i]+256 : b[i];
				System.out.println(x + ".");
			}
		}catch(UnknownHostException e){
			System.out.println(e);
		}

	}

}
