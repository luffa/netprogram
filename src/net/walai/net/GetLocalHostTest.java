package net.walai.net;
import java.net.*;
public class GetLocalHostTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			InetAddress a = InetAddress.getLocalHost();
			System.out.println("name: "+ a.getHostName());
			System.out.println("IP: "+ a.getHostAddress());		
						
		}catch(UnknownHostException e){
			System.out.println(e);
		}
	}

}
